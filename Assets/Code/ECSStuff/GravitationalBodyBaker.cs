using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class GravitationalBodyBaker : Baker<GravitationalBodyAuthoring>
{
    public override void Bake(GravitationalBodyAuthoring authoring)
    {
        var entity = GetEntity(TransformUsageFlags.Dynamic);
        AddComponent(entity, new GravitationalBodyComponent
        {
            kinematicsData = new KinematicsData
            {
                velocity = authoring.initialVelocity,
                acceleration = authoring.initialAcceleration
            },
            orbitalElements = new OrbitalElementsSimple
            {

            }
        });
    }
}
