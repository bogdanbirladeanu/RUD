using UnityEngine;

[DisallowMultipleComponent]
public class GravitationalBodyAuthoring : MonoBehaviour
{
    public Vector3 initialVelocity;
    public Vector3 initialAcceleration;
    public float semiMajorAxis;
    public float eccentricity;
    public float inclination;
    public float longitudeOfAscendingNode;
    public float argumentOfPeriapsis;
    public float trueAnomaly;
}