using NUnit.Framework;
using Shapes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Drawing;


public class TestKeplerBody : MonoBehaviour
{
    public Color gizmoColor = new Color(1.0f, 88 / 255f, 85 / 255f);
    public Transform parent;
    public int pointCount = 10;
    public double periapsis;
    public double apoapsis;
    public List<Vector3> simPoints;
    public List<Vector3> displayPoints = new List<Vector3>();
    public Polyline polyline;

    private void Start()
    {
        ComputePoints();
        DisplayPoints(1);
    }


    void ComputePoints()
    {
        simPoints = ComputePointsForOrbitalElements(KeplerSolver.ComputeOrbitalElements(periapsis, apoapsis));
        displayPoints.AddRange(simPoints);
    }

    public void DisplayPoints(float scalingFactor)
    {
        for (int i = 0; i < simPoints.Count; i++)
        {
            displayPoints[i] = PositionScaling.ScalePositionByExponentialFactor(simPoints[i], -scalingFactor);
        }

        transform.position = simPoints[0];
        polyline.SetPoints(displayPoints);
    }

    private List<Vector3> ComputePointsForOrbitalElements(OrbitalElementsSimple orbitalElements)
    {
        List<Vector3> points = new List<Vector3>();
        for (int i = 0; i < pointCount; i++)
        {
            double ratio = (double)i / (double)pointCount;
            Vector3 position = KeplerSolver.ComputePositionForOrbitalElements(orbitalElements, ratio);
            position = position + parent.position;
            points.Add(position);

        }
        return points;
    }


}
