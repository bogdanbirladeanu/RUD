using UnityEngine;
using Zenject;

public class DependenciesInstallerTest : MonoInstaller<DependenciesInstallerTest>
{
    public override void InstallBindings()
    {
        Container.Bind<CameraManager>().FromComponentInHierarchy().AsSingle();
    }
}
