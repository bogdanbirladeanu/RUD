using KnowledgeCore;
using Shapes;
using TMPro;
using UnityEngine;

public class SunDisplayController : MonoBehaviour
{
    //public PlanetDescription data;
    public Disc shape;
    public TextMeshPro text;

    private Color mainColor;

    private void Awake()
    {
        shape = GetComponentInChildren<Disc>();
        text = GetComponentInChildren<TextMeshPro>();
    }
    /*
    public void Initialise(PlanetDescription data)
    {
        this.data = data;
        mainColor = ColourUtility.HexToColor(data.orbitColor);
        text.transform.localPosition = new Vector3((float)data.planetRadius, (float)data.planetRadius, 0);
        text.text = data.name;
        text.color = mainColor;
        shape.Radius = (float)data.planetRadius;
        shape.Color = mainColor;
    }
    */
}
