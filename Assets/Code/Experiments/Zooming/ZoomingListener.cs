using UnityEngine;
using Zenject;

public class ZoomingListener : MonoBehaviour
{
    [Inject] OnZoomingSliderChanged zoomingSlider;
    KeplerianOrbitDisplayController orbitDisplayController;
    PlanetDisplayController planetDisplayController;
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    void Awake()
    {
        orbitDisplayController = GetComponent<KeplerianOrbitDisplayController>();
        planetDisplayController = GetComponent<PlanetDisplayController>();

    }
    private void Start()
    {
        zoomingSlider.onValueChangedAction += OnSliderValueChanged;
    }
    private void OnDestroy()
    {
        // Remove listener to avoid memory leaks
        zoomingSlider.onValueChangedAction -= OnSliderValueChanged;
    }
    public void OnSliderValueChanged(float sliderValue)
    {
        orbitDisplayController.DrawLine(sliderValue);
        planetDisplayController.DrawGraphics(sliderValue);
    }
}
