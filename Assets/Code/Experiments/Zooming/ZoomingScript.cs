using Shapes;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class ZoomingScript : MonoBehaviour
{
    public GameObject g1;
    public GameObject g2;
    public GameObject g3;
    public GameObject g4;

    public double3 simPosition1;
    public double3 simPosition2;
    public double3 simPosition3;
    public double3 simPosition4;

    public Slider slider;

    public double astronomicalUnit=1;

    public Polyline polyline;

    private Vector3 v1 = Vector3.zero;
    private Vector3 v2 = Vector3.zero;
    private Vector3 v3 = Vector3.zero;
    private Vector3 v4 = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float factor = slider.value;
        polyline.points.Clear();
        UpdatePosition(g1.transform, ref v1, in simPosition1, factor);
        UpdatePosition(g2.transform, ref v2, in simPosition2, factor);
        UpdatePosition(g3.transform, ref v3, in simPosition3, factor);
        UpdatePosition(g4.transform, ref v4, in simPosition4, factor);
    }

    void UpdatePosition(Transform obj, ref Vector3 displayPos, in double3 simPos, float factor)
    {
        PositionScaling.ScalePositionByExponentialFactor(out displayPos, in simPos, -factor);
        obj.position = displayPos;
        polyline.AddPoint(obj.position);
    }
}
