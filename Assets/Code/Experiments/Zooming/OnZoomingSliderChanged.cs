using UnityEngine;
using UnityEngine.UI;
using System;

public class OnZoomingSliderChanged : MonoBehaviour
{
    public Action<float> onValueChangedAction = delegate { };
    Slider slider;
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    void Awake()
    {
        slider = GetComponent<Slider>();
        slider.onValueChanged.AddListener(OnSliderValueChanged);
    }
    private void OnDestroy()
    {
        // Remove listener to avoid memory leaks
        slider.onValueChanged.RemoveListener(OnSliderValueChanged);
    }
    private void OnSliderValueChanged(float value)
    {

        // Invoke the action when the slider value changes
        onValueChangedAction?.Invoke(value);
    }
}
