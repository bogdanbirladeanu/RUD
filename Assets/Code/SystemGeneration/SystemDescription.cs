using System.Collections.Generic;
using System;
using UnityEngine;
using Unity.Mathematics;

[Serializable]
public class SystemData
{
    public List<StarData> starData = new List<StarData>();
    public SystemScaleData scaleData;
    public int systemId;
    public double gravitationalConstant;
}

[Serializable]
public class SystemScaleData
{
    public double orbitsScale;
    public double radiusScale;
    public double massScale; //exponent
}

[Serializable]
public class GravitationalBodyData
{
    public int bodyId;
    public double mass;
    public string name;
    public double apoapsis;
    public double periapsis;
    public string color;
    public double radius;
    public double atmosphere;//this should probably be something like: AtmosphereData, or could have this in a different file
    public OrbitalElements orbitalElements;
    public InitialKinematicStateData initialKinematicState;
}

[Serializable]
public class StarData : GravitationalBodyData
{

    //other star data stuff, composition, luminosity etc
    public List<PlanetData> orbitalBodies = new List<PlanetData>();
}


[Serializable]
public class PlanetData : GravitationalBodyData
{

    //add type of planet here 
    public List<MoonData> orbitalBodies = new List<MoonData>();
   // public List<StationData> stations = new List<StationData>();
}

[Serializable]
public class MoonData : GravitationalBodyData
{

    //add type of planet here 
   // public List<StationData> stationData = new List<StationData>();
}

[Serializable]
public class StationData
{
    public int stationId;
    public double mass;
    public string name;
}

[Serializable]
public class InitialKinematicStateData
{
    public double3 position;
    public double3 velocity;
}