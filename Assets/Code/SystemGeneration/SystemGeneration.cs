using Unity.Entities;
using UnityEngine;

public class SystemGeneration : MonoBehaviour
{
    private string filePath = "Json/SolarSystemTest";
    public SystemData systemDescription = new SystemData();
    public GameObject planetPrefab, sunPrefab;


    // Start is called once before the first execution of Update after the MonoBehaviour is created
    void Start()
    {
        LoadJson();
    }

    private void LoadJson()
    {
        systemDescription = JsonUtility.FromJson<SystemData>(Resources.Load<TextAsset>(filePath).text);

        GenerateSystem();
    }

    private void GenerateSystem()
    {
        //GameObject sun = GameObject.Instantiate(sunPrefab);

        //PlanetDescription sunData = new PlanetDescription();
        //sunData.orbitColor = systemDescription.color;
        //sunData.name = systemDescription.name;
        //sunData.planetRadius = systemDescription.sunRadius;

        //sun.GetComponent<SunDisplayController>().Initialise(sunData);

        foreach (StarData planetDescription in systemDescription.starData)
        {
            //GameObject planet = GameObject.Instantiate(planetPrefab);
            //planet.GetComponent<PlanetDisplayController>().Initialise(planetDescription);
        }
    }
}
