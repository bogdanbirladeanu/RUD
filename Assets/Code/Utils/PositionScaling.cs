using System;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UIElements;

public static class PositionScaling
{
    public static void ScalePositionByExponentialFactor(out Vector3 displayPos, in double3 simPos, double factor)
    {
        displayPos.x = (float)(simPos.x * math.pow(10, factor));
        displayPos.y = (float)(simPos.y * math.pow(10, factor));
        displayPos.z = (float)(simPos.z * math.pow(10, factor));
    }

    public static void ScaleLengthByExponentialFactor(out double displayPos, in double simPos, double factor)
    {
        displayPos = simPos * math.pow(10, factor);

    }

    public static Vector3 ScalePositionByExponentialFactor(in Vector3 simPos, double factor)
    {
        Vector3 returnVal = Vector3.zero;
        returnVal.x = (float)(simPos.x * math.pow(10, factor));
        returnVal.y = (float)(simPos.y * math.pow(10, factor));
        returnVal.z = (float)(simPos.z * math.pow(10, factor));
        return returnVal;
    }

    public static double3 ScaleVectorByExponentialFactor(in double3 vector, double factor)
    {
        return vector * math.pow(10, factor);
    }
}
