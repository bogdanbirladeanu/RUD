﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PoolUtility
{
    public static List<GameObject> GeneratePool(GameObject poolType, int poolCount, bool activeInit = false)
    {
        List<GameObject> returnValue = new List<GameObject>();
        for (int i = 0; i < poolCount; i++)
        {
            GameObject obj = GameObject.Instantiate(poolType);
            obj.SetActive(activeInit);
            returnValue.Add(obj);
        }
        return returnValue;
    }
    public static List<T> GeneratePool<T>(int poolCount) where T : new()
    {
        List<T> returnValue = new List<T>();
        for (int i = 0; i < poolCount; i++)
        {
            T obj = new T();
            returnValue.Add(obj);
        }
        return returnValue;
    }

}

public class ObjectPool
{
    private GameObject typeObject;
    private List<GameObject> objectList = new List<GameObject>();

    public ObjectPool(GameObject typeObject, int capacity)
    {
        this.typeObject = typeObject;
        objectList = PoolUtility.GeneratePool(typeObject, capacity);
    }

    public int GetCount()
    {
        return objectList.Count;
    }

    public void DisableAll()
    {
        for (int i = 0; i < objectList.Count; i++)
        {
            objectList[i].SetActive(false);
        }
    }
    public GameObject GetFirstAvailable()
    {
        for (int i = 0; i < objectList.Count; i++)
        {
            if (!objectList[i].activeSelf)
            {
                return objectList[i];
            }
        }
        /*GameObject newObject = GameObject.Instantiate(typeObject) ;
        objectList.Add(newObject);
        newObject.SetActive(false);
        return newObject;*/
        return null;
    }

}