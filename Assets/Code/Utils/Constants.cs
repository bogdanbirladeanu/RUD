﻿using System;
using UnityEngine;
namespace KnowledgeCore
{
    public static class Constants 
    {
        public static double Deg2Rad = Math.PI / 180;
        public static double GravitationalConstant = 0.0000000000667408d;
        public static readonly Vector2 IdentityVector2 = new Vector2(1, 1);
        public static readonly Vector3 IdentityVector3 = new Vector3(1, 1, 1);
        public static readonly Vector2 ZeroVector2 = new Vector2(0, 0);
        public static readonly Vector3 ZeroVector3 = new Vector3(0, 0, 0);
        public static readonly Vector2 UpVector2 = Vector2.up;
        public static readonly Vector3 UpVector3 = Vector3.up;
        public static readonly Vector2 DownVector2 = Vector2.down;
        public static readonly Vector3 DownVector3 = Vector3.down;
        public static readonly Vector2 LeftVector2 = Vector2.left;
        public static readonly Vector3 LeftVector3 = Vector3.left;
        public static readonly Vector3 ForwardVector3 = Vector3.forward;

        public static Vector2 RotateVector2(Vector2 initialValue, float angleRad)
        {
            Vector2 returnValue = Vector2.zero;
            returnValue.x = initialValue.x * Mathf.Cos(angleRad) - initialValue.y * Mathf.Sin(angleRad);
            returnValue.y = initialValue.x * Mathf.Sin(angleRad) + initialValue.y * Mathf.Cos(angleRad);
            return returnValue;
        }

    }
}
    
