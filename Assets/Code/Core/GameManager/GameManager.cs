using UnityEngine;

public abstract class GameManager : MonoBehaviour
{
    void Start()
    {
        InitialiseGame();
    }
    public abstract void InitialiseGame();
}
