using UnityEngine;
using Zenject;

public class CoreGameManager : GameManager
{
    [Inject] private DataLoader dataLoader;
    [Inject] private GravitationalSimulation gravitationalSimulation;
    [Inject] private SystemGraphicsManager systemGraphics;
    [Inject] private OnZoomingSliderChanged onZoomingSliderChanged;

    public override void InitialiseGame()
    {
        dataLoader.LoadData();
        gravitationalSimulation.Initialise();
        systemGraphics.Initialise();
    }

}
