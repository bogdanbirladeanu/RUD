using UnityEngine;
using Zenject;

public class CoreGameDependenciesInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<DataLoader>().AsSingle();
        Container.Bind<GravitationalSimulation>().AsSingle();
        Container.Bind<SystemGraphicsManager>().AsSingle();


        Container.Bind<GameManager>().FromComponentInHierarchy().AsSingle();

        Container.Bind<OnZoomingSliderChanged>().FromComponentInHierarchy().AsSingle();
        Container.Bind<OnTimeSliderChanged>().FromComponentInHierarchy().AsSingle();

    }
}