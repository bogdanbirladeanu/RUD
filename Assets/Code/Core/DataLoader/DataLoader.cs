using UnityEngine;

//TODO: if this gets unwieldy break it up in multiple classes or a generic
//TODO: at some point I will need to make this accept a filePath and load multiple systems
public class DataLoader
{
    public DataLoader()
    {

    }

    private SystemData systemData;
    private string systemFilePath = "Json/SolarSystemData";
    //private string systemFilePath = "Json/EarthSystem";

    //private ConfigData systemData;
    //private string systemFilePath = "Json/SolarSystemData";


    public void LoadData()
    {
        TextAsset jsonTextAsset = Resources.Load<TextAsset>(systemFilePath);
        if (jsonTextAsset != null)
        {
            systemData = JsonUtility.FromJson<SystemData>(jsonTextAsset.text);
        }
        else
        {
            Debug.LogError("Failed to load JSON file from " + systemFilePath);
        }
    }
    public SystemData GetSystemData()
    {
        return systemData;
    }
}
