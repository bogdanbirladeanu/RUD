using KnowledgeCore;
using System;
using System.Collections.Generic;
using Unity.Transforms;
using UnityEditor.Overlays;
using UnityEngine;
using Zenject;

public class GravitationalSimulation
{
    [Inject] private DataLoader dataLoader;

    private Dictionary<string, KeplerianBody> keplerianBodies = new Dictionary<string, KeplerianBody>();
    private Dictionary<string, string> stepIntegrationBodies = new Dictionary<string, string>();
    private SystemData systemData;

    public void Initialise()
    {
        systemData = dataLoader.GetSystemData();
        CreateSimulation(systemData);
    }

    public KeplerianBody GetKeplerianBody(string name)
    {
        return keplerianBodies[name];
    }
    public Dictionary<string, KeplerianBody> GetKeplerianBodyMap()
    {
        return keplerianBodies;
    }

    public SystemScaleData GetSystemScaleData()
    {
        return systemData.scaleData;
    }

    private void CreateSimulation(SystemData systemData)
    {
        Constants.GravitationalConstant = systemData.gravitationalConstant;

        StarData centralStar = systemData.starData[0];
        KeplerianBody star = CreateStar(centralStar, systemData.scaleData);
        foreach(PlanetData planet in centralStar.orbitalBodies)
        {
            CreatePlanet(planet, star, systemData.scaleData);
        }
    }

    private KeplerianBody CreateStar(StarData starData, SystemScaleData scaleData)
    {
        KeplerianBody body = new KeplerianBody(starData, null, scaleData);
        keplerianBodies.Add(starData.name, body);
        return body;
    }

    private void CreatePlanet(PlanetData planetData, KeplerianBody parent, SystemScaleData scaleData)
    {
        KeplerianBody body = new KeplerianBody(planetData, parent, scaleData);
        keplerianBodies.Add(planetData.name, body);
        foreach (MoonData moon in planetData.orbitalBodies)
        {
            CreateMoon(moon, body, scaleData);
        }
    }

    private void CreateMoon(MoonData moonData, KeplerianBody parent, SystemScaleData scaleData)
    {
        KeplerianBody body = new KeplerianBody(moonData, parent, scaleData);
        keplerianBodies.Add(moonData.name, body);
    }
}

