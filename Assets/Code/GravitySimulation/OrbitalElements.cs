using System;
using UnityEngine;

[Serializable]
public class OrbitalElements
{
    public double semiMajorAxis;
    public double eccentricity;
    public double inclination;
    public double longAccendingNode;
    public double argOfPeriapsis;
    public double meanAnomaly;
}
