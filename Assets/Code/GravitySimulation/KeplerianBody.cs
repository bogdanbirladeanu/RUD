using KnowledgeCore;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class KeplerianBody
{
    public GravitationalBodyData bodyData;
    public KeplerianBody parent;
    public OrbitalElementsSimple orbitalElementsSimple;
    public OrbitalElements orbitalElements;
    public Vector3 position;
    public Vector3 velocity;

    public double orbitalPeriod;
    public double mu;

    public KeplerianBody(GravitationalBodyData bodyData, KeplerianBody parent, SystemScaleData scaleData)
    {
        this.bodyData = bodyData;
        this.parent = parent;

        if (isStatic()) return;

        orbitalElements = ApplyProcessing(bodyData.orbitalElements, scaleData);

        mu = Constants.GravitationalConstant * parent.bodyData.mass;
        orbitalPeriod = 2.0d * Math.PI * Math.Sqrt(Math.Pow(orbitalElements.semiMajorAxis, 3.0d) / mu);

        position = CalculatePositionAtTime(0);
    }

    private OrbitalElements ApplyProcessing(OrbitalElements orbitalElements, SystemScaleData scaleData)
    {

        OrbitalElements returnValue = orbitalElements;
        returnValue.semiMajorAxis *= scaleData.orbitsScale;
        //returnValue.inclination *= Constants.Deg2Rad;
        //returnValue.longAccendingNode *= Constants.Deg2Rad;
        //returnValue.argOfPeriapsis *= Constants.Deg2Rad;
        returnValue.meanAnomaly *= Constants.Deg2Rad;
        return returnValue;
    }

    public Vector3 CalculatePositionAtTime(double t)
    {
        double n = 2 * Math.PI / orbitalPeriod;
        double M = orbitalElements.meanAnomaly + n * t;
        double E = SolveKeplerEquation(M, orbitalElements.eccentricity);
        double v = 2 * Math.Atan2(Math.Sqrt(1 + orbitalElements.eccentricity) * Math.Sin(E / 2), Math.Sqrt(1 - orbitalElements.eccentricity) * Math.Cos(E / 2));

        double r = orbitalElements.semiMajorAxis * (1 - orbitalElements.eccentricity * Math.Cos(E));

        Vector3 perifocalPosition = new Vector3((float)(r * Math.Cos(v)), (float)(r * Math.Sin(v)), (float)0);

        position = RotateToInertialFrame(perifocalPosition) + parent.position;
        return position;
    }

    public Vector3 CalculatePositionAtRatio(double ratio)
    {
        double t = ratio * orbitalPeriod;
        double n = 2 * Math.PI / orbitalPeriod;
        double M = orbitalElements.meanAnomaly + n * t;
        double E = SolveKeplerEquation(M, orbitalElements.eccentricity);
        double v = 2 * Math.Atan2(Math.Sqrt(1 + orbitalElements.eccentricity) * Math.Sin(E / 2), Math.Sqrt(1 - orbitalElements.eccentricity) * Math.Cos(E / 2));

        double r = orbitalElements.semiMajorAxis * (1 - orbitalElements.eccentricity * Math.Cos(E));

        Vector3 perifocalPosition = new Vector3((float)(r * Math.Cos(v)), (float)(r * Math.Sin(v)), (float)0);
        position = RotateToInertialFrame(perifocalPosition) + parent.position;
        return position;
    }

    public Vector3 CalculateVelocityAtTime(double t)
    {
        double n = 2 * Math.PI / orbitalPeriod;
        double M = orbitalElements.meanAnomaly + n * t;
        double E = SolveKeplerEquation(M, orbitalElements.eccentricity);
        double v = 2 * Math.Atan2(Math.Sqrt(1 + orbitalElements.eccentricity) * Math.Sin(E / 2), Math.Sqrt(1 - orbitalElements.eccentricity) * Math.Cos(E / 2));

        double r = orbitalElements.semiMajorAxis * (1 - orbitalElements.eccentricity * Math.Cos(E));

        double rDot = Math.Sqrt(mu / orbitalElements.semiMajorAxis) * orbitalElements.eccentricity * Math.Sin(E) / (1 - orbitalElements.eccentricity * Math.Cos(E));
        double rNuDot = Math.Sqrt(mu * orbitalElements.semiMajorAxis) / r;

        Vector3 perifocalVelocity = new Vector3((float)(rDot * Math.Cos(v) - rNuDot * Math.Sin(v)), (float)(rDot * Math.Sin(v) + rNuDot * Math.Cos(v)), 0);

        return RotateToInertialFrame(perifocalVelocity);
    }

    double SolveKeplerEquation(double M, double e, int maxIterations = 50, double tolerance = 1e-6f)
    {
        double E = M;
        for (int iter = 0; iter < maxIterations; iter++)
        {
            double delta = (E - e * Math.Sin(E) - M) / (1 - e * Math.Cos(E));
            E -= delta;
            if (Math.Abs(delta) < tolerance)
                break;
        }
        return E;
    }


    Vector3 RotateToInertialFrame(Vector3 perifocalVector)
    {
        double i = orbitalElements.inclination;
        double om = orbitalElements.longAccendingNode;
        double w = orbitalElements.argOfPeriapsis;

        Quaternion rotation = Quaternion.Euler(0, 0, (float)-om) * Quaternion.Euler((float)-i, 0, 0) * Quaternion.Euler(0, 0, (float)-w);
        return rotation * perifocalVector;
    }

    public List<Vector3> ComputeOrbit(int pointCount)
    {
        List<Vector3> points = new List<Vector3>();
        for (int i = 0; i < pointCount; i++)
        {
            double ratio = (double)i / (double)pointCount;
            //Vector3 position = KeplerSolver.ComputePositionForOrbitalElements(orbitalElements, ratio);
            double t = ratio * orbitalPeriod;
            Vector3 position = CalculatePositionAtTime(t);
            points.Add(position);

        }
        return points;
    }

    public bool isStatic()
    {
        return bodyData.periapsis == 0 && bodyData.apoapsis == 0;
    }
}

