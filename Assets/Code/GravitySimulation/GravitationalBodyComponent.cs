using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public struct GravitationalBodyComponent : IComponentData
{
    public KinematicsData kinematicsData;
    public OrbitalElementsSimple orbitalElements;


}
