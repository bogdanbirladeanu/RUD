
using Unity.Mathematics;
public struct OrbitalElementsSimple
{
    public double semiMajorLength;
    public double eccentricity;
    public double linearEccentricity;
    public double semiMinorLength;

    public OrbitalElementsSimple(double semiMajorLength, double eccentricity, double linearEccentricity, double semiMinorLength)
    {
        this.semiMajorLength = semiMajorLength;
        this.eccentricity = eccentricity;  
        this.linearEccentricity = linearEccentricity;
        this.semiMinorLength = semiMinorLength;
    }



}
