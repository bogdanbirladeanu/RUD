
using Unity.Mathematics;
public struct KinematicsData
{
    public float3 position;
    public float3 velocity;
    public float3 acceleration;
}
