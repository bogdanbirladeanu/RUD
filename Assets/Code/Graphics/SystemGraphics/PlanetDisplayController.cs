using KnowledgeCore;
using Shapes;
using TMPro;
using UnityEngine;

public class PlanetDisplayController : ImmediateModeShapeDrawer
{
    public GravitationalBodyData data;
    public Disc shape;
    public TextMeshPro text;
    public TextElement elemA;
    private Color mainColor;
    private double bodyRadius;
    private void Awake()
    {
        shape = GetComponentInChildren<Disc>();
        text = GetComponentInChildren<TextMeshPro>();
        elemA = new TextElement();
    }

    public void Initialise(GravitationalBodyData data, SystemScaleData scaleData)
    {
        this.data = data;
        bodyRadius = data.radius * scaleData.radiusScale;

        mainColor = ColourUtility.HexToColor(data.color);
        //text.transform.localPosition = new Vector3((float)bodyRadius, (float)bodyRadius, 0);
        text.transform.LookAt(Camera.main.transform);
        text.text = data.name;
        text.color = mainColor;
        shape.Radius = (float)bodyRadius;
        shape.Color = mainColor;

    }

    public void DrawGraphics(float scalingFactor)
    {
        double radius = bodyRadius;
        //PositionScaling.ScaleLengthByExponentialFactor(out radius, in bodyRadius, -scalingFactor);
        shape.Radius = (float) radius;
        text.transform.localPosition = new Vector3((float)radius, (float)radius, 0);

    }
    /*
    public override void DrawShapes(Camera cam)
    {

        using (Draw.Command(cam))
        {
            Draw.ThicknessSpace = ThicknessSpace.Pixels;
            Draw.LineGeometry = LineGeometry.Billboard;
            Draw.Text(elemA, pos: transform.position, data.name, fontSize: 36);
        }
    }*/
}
