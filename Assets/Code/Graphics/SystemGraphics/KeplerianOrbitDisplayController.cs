using KnowledgeCore;
using Shapes;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class KeplerianOrbitDisplayController : ImmediateModeShapeDrawer
{
    [Header("Line Style")][Range(1, 2)] public float lineThickness = 1f;
    [Header("Point Count")][Range(6, 150)] public int pointCount = 150;

    public OrbitalElements orbitalElements;
    
    private PlanetDisplayController planetDisplayController;
    public KeplerianBody keplerianBody;

    public List<Vector3> displayPoints = new List<Vector3>();
    private Color lineColor;
    private double timeRatio = 1;
    void Awake()
    {
        planetDisplayController = GetComponent<PlanetDisplayController>();

    }

    public void Initialise(KeplerianBody keplerianBody)
    {
        this.keplerianBody = keplerianBody;
        this.orbitalElements = keplerianBody.orbitalElements;

        lineColor = ColourUtility.HexToColor(keplerianBody.bodyData.color);

        DrawLine(1);
        transform.position = keplerianBody.position;

    }

    public void UpdatePositionAtTime(float timeRatio)
    {
        if (keplerianBody == null || keplerianBody.isStatic()) return;
        transform.position = keplerianBody.CalculatePositionAtRatio(timeRatio);
    }
    public void UpdateTimeScale(float timeRatio)
    {
        this.timeRatio = timeRatio;
    }
    private void Update()
    {
        if (keplerianBody == null || keplerianBody.isStatic()) return;
        transform.position = keplerianBody.CalculatePositionAtTime(Time.time * timeRatio);
        DrawLine(1);
    }
    public void DrawLine(float scalingFactor)
    {
        if (keplerianBody.isStatic()) return;

        //List<Vector3> simPoints = keplerianBody.ComputeOrbit(pointCount);
        //displayPoints.Clear();
        /*
        for (int i = 0; i < simPoints.Count; i++)
        {
            displayPoints.Add(PositionScaling.ScalePositionByExponentialFactor(simPoints[i], -scalingFactor));
        }
        */
        //displayPoints = simPoints;

        //UpdatePositionAtTime(0);
    }

    public override void DrawShapes(Camera cam)
    {
        /*
        // Draw.Command enqueues a set of draw commands to render in the given camera
        using (Draw.Command(cam))
        { // all immediate mode drawing should happen within these using-statements

            // Set up draw state
            Draw.ResetAllDrawStates(); // ensure everything is set to their defaults
            Draw.BlendMode = ShapesBlendMode.Additive;
            Draw.Thickness = lineThickness;
            Draw.LineGeometry = LineGeometry.Volumetric3D;
            Draw.ThicknessSpace = ThicknessSpace.Meters;
            Draw.Color = lineColor;
            for(int i = 0; i < displayPoints.Count-1; i++)
            {
                Draw.Line(displayPoints[i], displayPoints[i+1]);
            }

        }*/
        using (Draw.Command(cam))
        {
            using (var p = new PolylinePath())
            {
                p.AddPoints(displayPoints);
                Draw.ThicknessSpace = ThicknessSpace.Pixels;
                Draw.Polyline(p, closed: true, thickness: lineThickness, lineColor ); // Drawing happens here
            } // Disposing of mesh data happens here
        }
    }


}
