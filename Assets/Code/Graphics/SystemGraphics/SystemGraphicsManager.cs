using System;
using System.Collections.Generic;
using System.ComponentModel;
using Unity.Entities;
using UnityEngine;
using Zenject;

public class SystemGraphicsManager
{
    [Inject] readonly DiContainer container;
    [Inject] private GravitationalSimulation gravitationalSimulation;

    private string planetPrefabPath = "Prefabs/Planet";
    private GameObject planetPrefab;

    public void Initialise()
    {
        LoadPrefabs();
        InitialiseGraphicsForAllBodies();
    }

    private void LoadPrefabs()
    {
        planetPrefab = Resources.Load(planetPrefabPath) as GameObject;
    }

    private void InitialiseGraphicsForAllBodies()
    {
        Dictionary<string, KeplerianBody> bodies = gravitationalSimulation.GetKeplerianBodyMap();
        foreach (var body in bodies)
        {
            InitialiseGraphics(body.Value);
        }
    }

    private void InitialiseGraphics(KeplerianBody body)
    {
        GameObject planetGraphics = container.InstantiatePrefab(planetPrefab);

        planetGraphics.name = body.bodyData.name;
        //planetGraphics.transform.position = body.position;
        planetGraphics.GetComponent<PlanetDisplayController>().Initialise(body.bodyData, gravitationalSimulation.GetSystemScaleData());
        planetGraphics.GetComponent<KeplerianOrbitDisplayController>().Initialise(body);
    }
}
