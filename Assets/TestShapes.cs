using Shapes;
using UnityEngine;

public class TestShapes : ImmediateModeShapeDrawer
{
    [Header("Line Style")][Range(0.1f, 0.5f)] public float lineThickness = 0.2f;
    public Color lineColor = Color.white;
    public override void DrawShapes(Camera cam)
    {
        using (Draw.Command(cam))
        {
            using (var p = new PolylinePath())
            {
                p.AddPoint(-50, -50);
                p.AddPoint(-50, 50);
                p.AddPoint(50, 50);
                p.AddPoint(50, -50);
                Draw.Polyline(p, closed: true, thickness: lineThickness, lineColor); // Drawing happens here
            } // Disposing of mesh data happens here
        }
    }
}
