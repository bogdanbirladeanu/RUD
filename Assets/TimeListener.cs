using UnityEngine;
using Zenject;

public class TimeListener : MonoBehaviour
{
    [Inject] OnTimeSliderChanged timeSlider;
    KeplerianOrbitDisplayController orbitDisplayController;
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    void Awake()
    {
        orbitDisplayController = GetComponent<KeplerianOrbitDisplayController>();

    }
    private void Start()
    {
        timeSlider.onValueChangedAction += OnSliderValueChanged;
    }
    private void OnDestroy()
    {
        // Remove listener to avoid memory leaks
        timeSlider.onValueChangedAction -= OnSliderValueChanged;
    }
    public void OnSliderValueChanged(float sliderValue)
    {
        //orbitDisplayController.UpdatePositionAtTime(sliderValue);
        orbitDisplayController.DrawLine(1);
        orbitDisplayController.UpdateTimeScale(sliderValue);
    }
}
